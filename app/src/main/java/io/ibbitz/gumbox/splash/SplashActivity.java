package io.ibbitz.gumbox.splash;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import io.ibbitz.gumbox.R;
import io.ibbitz.gumbox.title.TitleActivity;

public class SplashActivity extends Activity {

    private static final int SPLASH_LENGTH_MILLIS = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        //Sets a timer to show the Gumbox logo for a couple seconds
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent toTitle = new Intent(SplashActivity.this, TitleActivity.class);
                startActivity(toTitle);
                finish();
            }
        }, SPLASH_LENGTH_MILLIS);
    }
}
