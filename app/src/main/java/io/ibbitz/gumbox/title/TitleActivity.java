package io.ibbitz.gumbox.title;

import android.app.Activity;
import android.os.Bundle;

import io.ibbitz.gumbox.R;

public class TitleActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_title);
    }
}
